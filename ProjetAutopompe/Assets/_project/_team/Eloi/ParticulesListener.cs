﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticulesListener : MonoBehaviour {

    public int collisionCount;
    [System.Serializable]
    public class CollisionCountEvent : UnityEngine.Events.UnityEvent<int>
    {}
    public CollisionCountEvent onCollisionEnter;

    private void OnParticleCollision(GameObject other)
    {
        AddCount();
    }

    public void OnParticleTrigger()
    {
        AddCount();

    }
    private void AddCount()
    {
        collisionCount++;
        onCollisionEnter.Invoke(collisionCount);
    }

}
