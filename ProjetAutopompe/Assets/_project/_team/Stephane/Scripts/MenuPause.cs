﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class MenuPause : MonoBehaviour
{
    private bool isPaused = false;
    public GameObject panelPause;
    // Use this for initialization    

    void Start() { }
    // Update is called once per frame     

    void Update() { }
    public void SwitchPause()
    {
        if (Time.timeScale > 0f)
        {
            Time.timeScale = 0;
            panelPause.SetActive(true);
        }

        else { Time.timeScale = 1; }
    }
}
