﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour {

    public GameObject panelPause;

public void PlayBtn(string PlayScene)
    {
        SceneManager.LoadScene(PlayScene);

    }
    public void ExitBtn()
    {
        Application.Quit();
    }

    public void ContinueBtn()
    {
        panelPause.SetActive(false);
        if(Time.timeScale == 0)
        Time.timeScale = 1;
    }

}


