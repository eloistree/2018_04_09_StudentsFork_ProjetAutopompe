﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowVuforiaCamera : MonoBehaviour {

    public Transform _vuforiaCamera;
    public float _lerpFactor=1;

    public float _backwardAdjustment = 10;

    void Update () {
        transform.localPosition = Vector3.Lerp(transform.localPosition, _vuforiaCamera.position + (_vuforiaCamera.forward* -1f *_backwardAdjustment), Time.deltaTime* _lerpFactor) ;
        transform.localRotation= Quaternion.Lerp(transform.localRotation, _vuforiaCamera.rotation, Time.deltaTime * _lerpFactor);


    }
}
