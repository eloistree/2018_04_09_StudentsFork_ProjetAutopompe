﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterParticulesSystem : MonoBehaviour {

    [SerializeField]
    [Range(0,1)]
    private float _pourcent;
    public float _minSpeed=62;
    public float _maxSpeed=90;
    public ParticleSystem [] _particule;
    //public SimpleTouchController rightController;
    //public Vector2 touchposition;

    public void SetPourcentPower(float pourcent) {

        //touchposition = rightController.GetTouchPosition;
        _pourcent = Mathf.Clamp01(pourcent);
        for (int i = 0; i < _particule.Length; i++)
        {

            ParticleSystem.MainModule speedModule = _particule[i].main;
            speedModule.startSpeed = _minSpeed + (_maxSpeed - _minSpeed) * _pourcent;
        }

    }
    public void OnValidate()
    {
        SetPourcentPower(_pourcent);
    }
}
