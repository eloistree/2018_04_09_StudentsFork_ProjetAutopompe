﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchModel : MonoBehaviour {

    public GameObject[] models;
    public int index;
    public int startIndex;
    bool check = false;

    public void Awake()
    {
        Activate(startIndex);
    }
    public void Activate(int index) {
        if (enabled == false) return;
        DisableAll();
        ActivateModel(index);
    }


    // Use this for initialization
    public void SwitchNext()
    {
        if (enabled == false) return;
        Activate(index);
        //if (check)
        //{
        index++;
        //}
        //check = true;
        ClampBetweenModels(index);
    }
    // Update is called once per frame
    public void Instanciate()
    {

    }

    private void DisableAll()
    {
        for (int i = 0; i < models.Length; i++)
        {
            models[i].SetActive(false);
        }
    }


    private void ActivateModel(int index)
    {

        this.index = ClampBetweenModels(index);
        
        models[this.index].SetActive(true);

    }

    private int ClampBetweenModels(int index)
    {
        // return Mathf.Clamp(index, 0, models.Length-1);
        return index < models.Length ? index : 0;
    }



}
