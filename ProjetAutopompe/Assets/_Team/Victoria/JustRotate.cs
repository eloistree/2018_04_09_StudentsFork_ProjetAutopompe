﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JustRotate : MonoBehaviour {
    public float speed=180;
    public Vector3 euleurDirection = Vector3.one;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        transform.Rotate(euleurDirection, speed * Time.deltaTime);
		
	}
}
