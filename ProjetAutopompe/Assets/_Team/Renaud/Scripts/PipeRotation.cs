﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PipeRotation : MonoBehaviour
{

    Ray ray = new Ray();
    public float rayLength;
    public LayerMask _layerMask;
    public Camera camera;

    void Start()
    {
        if (camera == null)
        {
            camera = Camera.main;
        }
    }

    void Update()
    {
        //Version Android
        if (Input.GetMouseButtonDown(0))
        {
            /// pipe.transform.Rotate(0, 90, 0);
            /// 
            RaycastHit hit;
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100000, _layerMask.value))
            {
                Debug.DrawRay(ray.origin, ray.direction * 10, Color.red, 10);
                SwitchModel switcher = hit.collider.GetComponent<SwitchModel>();
                if (switcher == null)
                {
                    switcher = hit.collider.gameObject.GetComponentInParent<SwitchModel>();
                }
                if (switcher != null)
                {
                    switcher.SwitchNext();

                }
            }

        }
    }
}
