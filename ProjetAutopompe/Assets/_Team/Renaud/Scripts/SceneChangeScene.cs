﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChangeScene : MonoBehaviour {


    public string scene;
    
    public void SceneChange()
    {
        SceneManager.LoadScene(scene);
    }
}
