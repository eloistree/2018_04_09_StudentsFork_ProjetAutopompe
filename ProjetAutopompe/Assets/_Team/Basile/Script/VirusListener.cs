﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class VirusListener : MonoBehaviour {


    public UnityEvent virusWon;
    public UnityEvent virusLost;
    public string scene;
     
    private void Awake()
    {
        WaterVirusNemesis.virusHasWon += DisplayVirusVictory;
        WaterVirusNemesis.virusHasLost += DisplayVirusLoss;
    }

    private void DisplayVirusLoss()
    {
        Debug.Log("virus lost");
        virusLost.Invoke();
    }

    private void DisplayVirusVictory()
    {
        Debug.Log("virus won");
        virusWon.Invoke();
        StartCoroutine("Changescene");
        SceneManager.LoadScene(scene);
    }
    IEnumerator Changescene()
    {
        yield return new WaitForSeconds(3);    
    }
}
