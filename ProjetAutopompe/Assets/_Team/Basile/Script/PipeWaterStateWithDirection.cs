﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeWaterStateWithDirection : PipeWaterState {

    public Transform from;
    public Transform negativeAnchor;
    public Transform positiveAnchor;
    public bool inverseDirection = false;
    public override float CompletionPercent
    {
        get { return _completionPercent; }
        set
        {
            if (from == null)
            {
                ChangeMatrialsSet(_completionPercent );
                //Debug.Log("from necesary", this.gameObject);
                return;
            }
            float negativeDistance = Vector3.Distance(from.position, negativeAnchor.position);
            float positiveDistance = Vector3.Distance(from.position, positiveAnchor.position); ;
            
            _completionPercent = Mathf.Clamp(value, 0, 1f);
            ChangeMatrialsSet(_completionPercent* ((negativeDistance > positiveDistance ? 1f : -1f)* (inverseDirection?-1f:1f)) );
        }
    }
   

    public void SetFromPos(Transform transform)
    {
        from = transform;
    }

    public void Start()
    {
        CompletionPercent = 0;
    }
}
