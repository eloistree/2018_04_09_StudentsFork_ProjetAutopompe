﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterVirusNemesis : MonoBehaviour
{
    public delegate void virusEvent();

    public static virusEvent virusHasWon;
    public static virusEvent virusHasLost;
    public PipeWaterStateWithDirection waterState;
    public float propagationSpeed = 1;
    public SceneChangeScene sceneChangeScene;

    public bool hasFinished;


    // Update is called once per frame
    void Update()
    {
        waterState.CompletionPercent += Time.deltaTime * propagationSpeed;
        if (!hasFinished && waterState.CompletionPercent >= 1f)
        {
            hasFinished = true;
            Pipe pipe = GetComponentInParent<Pipe>();
            List<Pipe> pipes = pipe.GetNearActivePipesLinked();

            bool hasContamined = false;

            foreach (var item in pipes)
            {
                if (!IsContaminated(item))
                {
                    if (!hasContamined)
                    {
                        hasContamined = true;
                    }
                    Contaminate(item, this.propagationSpeed);
                }
            }
            if (!hasContamined)
            {
                if(virusHasWon!=null)
                virusHasWon();
            }

        }
    }

    public static void Contaminate(Pipe pipe, float propagationSpeed=1f)
    {

        pipe.SetSwitchActive(false);
        WinnerPipeTag winTag = pipe.GetComponentInChildren<WinnerPipeTag>();
        if (winTag != null)
        {
            if (virusHasLost != null)
            {
                virusHasLost();
            }
        }

        PipeWaterStateWithDirection[] states = pipe.GetComponentsInChildren<PipeWaterStateWithDirection>();
        foreach (var item in states)
        {
            if (item.gameObject.GetComponent<WaterVirusNemesis>() == null)
            {
                item.gameObject.AddComponent<WaterVirusNemesis>();
                WaterVirusNemesis virus = item.gameObject.GetComponent<WaterVirusNemesis>();
                virus.waterState = item;
                virus.propagationSpeed = propagationSpeed;
                item.SetFromPos(pipe.transform);
            }
        }
    }

    public bool IsContaminated(Pipe pipe)
    {
        return pipe.GetActivePipeModel().gameObject.GetComponentInChildren<WaterVirusNemesis>() != null;
    }

}
