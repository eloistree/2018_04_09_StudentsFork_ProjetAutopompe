﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeLord : MonoBehaviour {

   
    public float _timeSpeed=5f;
	// Use this for initialization
	void Start () {
        ChangeTime();
	}

    private void ChangeTime()
    {
        Time.timeScale = _timeSpeed;
    }

    // Update is called once per frame
    void OnValidate () {

        ChangeTime();
    }
}
