﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContaminatePatientZero : MonoBehaviour {

    public float timeToContaminate=5f;
    public float speedOfPropagation=0.2f;
    public Pipe toContaminate;
	// Use this for initialization
	void Start () {
        Invoke("Contaminate", timeToContaminate);
	}
	
	// Update is called once per frame
	void Contaminate() {

        WaterVirusNemesis.Contaminate(toContaminate, speedOfPropagation);
	}
}
