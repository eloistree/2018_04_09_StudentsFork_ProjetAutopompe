﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeWaterState : MonoBehaviour
{
    public float waterUpdateRate;
    [Range(0, 1)]
    [SerializeField]
    protected float _completionPercent;
    public virtual float CompletionPercent
    {
        get { return _completionPercent; }
        set
        {
            _completionPercent = Mathf.Clamp(value,0,1f);
            ChangeMatrialsSet(_completionPercent);
        }
    }
    
    [SerializeField]
    private Renderer _waterRenderer;
    public Renderer WaterRenderer
    {
        get { return _waterRenderer; }
        set { _waterRenderer = value; }
    }
    protected void ChangeMatrialsSet(float percent)
    {

        if (!Application.isPlaying) return; //Allows to change offset textures on play mode only
       WaterRenderer.material.SetTextureOffset("_MainTex", new Vector2(percent / 2f + 0.5f, 0));
    }

    private void OnValidate()
    {
        CompletionPercent = _completionPercent;
    }

    void MoveWater()
    {
        CompletionPercent += 0.001f;
    }

    private void Start()
    {
        _completionPercent = 0;
        InvokeRepeating("MoveWater",0,waterUpdateRate);       
    }
}
