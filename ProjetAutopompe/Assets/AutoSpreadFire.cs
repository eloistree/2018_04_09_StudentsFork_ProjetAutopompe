﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoSpreadFire : MonoBehaviour {

    

    public float minTimeBeforeSpawn = 3;
    public float maxTimeBeforeSpawn = 6;

    public float range = 5;
    public LayerMask colliderWith;
    public LayerMask fireLayer;

    public int maxFireSpread = 3;
    public float nearFireRange = 15;
    public float propagationDistance=5;

    // Use this for initialization
    IEnumerator Start()
    {
        while (true)
        {
           float time = UnityEngine.Random.Range(minTimeBeforeSpawn, maxTimeBeforeSpawn);

            yield return new WaitForSeconds(time);
            Vector3 whereToSpawn;//= GetRandomPositionAround();
            whereToSpawn= GetOpposedDirection( GetAveragePointOf( GetNearFire(range)), transform.position);
            whereToSpawn += GetRandomPointInFireRange();
            bool succes = GenerateFireRandomly(whereToSpawn);

            if (succes) {

                if (maxFireSpread < 0)
                    break;
                else maxFireSpread--;
            }
        };
    }

    private Vector3 GetRandomPositionAround()
    {
        return transform.position + GetRandomPointInFireRange();
    }

    private Vector3 GetRandomPointInFireRange()
    {
        return new Vector3(RandomRange(), RandomRange(), RandomRange());
    }

    public float RandomRange() {
        return UnityEngine. Random.Range(-range, range);
    }
    public bool GenerateFireRandomly(Vector3 whereToGenerate)
    {
        GameObject fireInst = FireGenerator.InstanceInTheScene.GenerateFire(whereToGenerate);
        if (fireInst == null)
            return false;
        //fireInst.transform.parent = this.transform;

        Vector3 whereToSpawn = whereToGenerate;// transform.position + new Vector3(Random.Range(minX, maxX), 10000, Random.Range(minZ, maxZ));
        whereToSpawn.y = 10000;
        RaycastHit result;
        if (Physics.Raycast(whereToSpawn, Vector3.down, out result, float.MaxValue, colliderWith))
        {
            whereToSpawn = result.point;
        }
        else
        {
            whereToSpawn.z =
            this.transform.position.z;
        }
        fireInst.transform.position = whereToSpawn;
        return true;
    }

    public List<GameObject> GetNearFire(float range) {
       RaycastHit []  hots=   Physics.SphereCastAll(transform.position, nearFireRange,Vector3.one* 0.01f, fireLayer);
        List<GameObject> fires = new List<GameObject>();
        for (int i = 0; i < hots.Length; i++)
        {
            fires.Add(hots[i].collider.gameObject);

        }
        return fires;
    }

    public Vector3 GetAveragePointOf(List<GameObject> targets) {
        Vector3 result = Vector3.zero;
        for (int i = 0; i < targets.Count; i++)
        {
            result += targets[i].transform.position;
        }
        result /= targets.Count;
        return result;
    }

    public Vector3 GetOpposedDirection(Vector3 toOppose, Vector3 currentPosition) {
        Vector3 result = Vector3.zero;

        result =  currentPosition + (currentPosition - toOppose).normalized * propagationDistance;
        Debug.DrawLine(toOppose, result,Color.red, 10);

        return result;
    }
}
